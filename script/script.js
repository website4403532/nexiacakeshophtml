window.onload = function () {
    $(function () {
        $(".rslides").responsiveSlides();
    });
    
    $(document).ready(function () {
        $("a").on('click', function(event) {

            // Make sure this.hash has a value before overriding default behavior
            if (this.hash !== "") {
              // Prevent default anchor click behavior
              event.preventDefault();
        
              // Store hash
              var hash = this.hash;
        
              // Using jQuery's animate() method to add smooth page scroll
              // The optional number (800) specifies the number of milliseconds it takes to scroll to the specified area
              $('html, body').animate({
                scrollTop: $(hash).offset().top
              }, 800, function(){
           
                // Add hash (#) to URL when done scrolling (default click behavior)
                window.location.hash = hash;
              });
            } // End if
          });
    })

    let submit = document.getElementById("submit");
    function validateEmail(str){
        at = -1;
        dot = -1;
    
        for(i = 0; i< str.length; i++){
            if(str.charAt(i) == '@')
                at = i;
    
            if(str.charAt(i) == '.')
                dot = i;
        }
    
        console.log(at);
        console.log(dot);
    
        if(at == -1 || dot == -1)
            return false;
    
        if(str.substr(0, at) == "") return false;
    
        if(dot - at <= 1) return false;
        
        if(str.substr(dot, str.length) == "") return false;
        
        return true;
    }


    let validate = (e) => {
        let fullname = document.getElementById("fullname").value;
        let email = document.getElementById("email").value;
        let phonenumber = document.getElementById("phonenumber").value;
        let message = document.getElementById("message").value;
        var numbers = /^[0-9]+$/;

        e.preventDefault();

        if (fullname == '') {
            alert('Fullname must be filled!');
        } else if (email == '') {
            alert('E-mail must be filled!');
        } else if (validateEmail(email) == false) {
            alert('E-mail must Contain @ and .');
        } else if (phonenumber == '') {
            alert('Phone Number must be Filled!')
        } else if (!phonenumber.match(numbers)) {
            alert('Phone Number only Numbers!');
        } else if (phonenumber.length > 12) {
            alert('Phone Number max. 12 characters!')
        } else if (message == '') {
            alert('Message must be Filled!');
        } else if (message.length < 10) {
            alert('Message must 10 characters minimals!');
        } else {
            alert('Success')
        }
    }
    submit.addEventListener("click", e => validate(e))
}
